package com.sspecht.tdchallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void onBackClick(View view)
    {
        finish();
    }

    // Override back press so that it always takes us back to the Scanner activity
    @Override
    public void onBackPressed(){finish();}

}
