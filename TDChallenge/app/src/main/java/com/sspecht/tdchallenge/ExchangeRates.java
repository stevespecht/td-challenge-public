package com.sspecht.tdchallenge;

public class ExchangeRates {

    String targetCurrency;
    double rate;

    public ExchangeRates(String targetCurrency, double rate)
    {
        this.targetCurrency = targetCurrency;
        this.rate = rate;
    }
}
