package com.sspecht.tdchallenge;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity implements MoneyAdapter.ItemClickListener, GestureDetector.OnGestureListener {


    private GestureDetector gestureDetector;
    Spinner spTargetCurrency, spDialogCurrency, spDialogValue;
    TextView tvViewConvertSum;
    ArrayList<String> allScans, currencies, values;
    ArrayList<ExchangeRates> exchangeRates;
    ArrayList<Money> allMoney;
    ArrayAdapter<String> spinnerAdapter;

    Context context;
    RecyclerView recyclerView;
    MoneyAdapter adapter;
    AlertDialog dialog;
    Money selectedMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.right_slide_start, R.anim.right_slide_end);
        setContentView(R.layout.activity_result);

        gestureDetector = new GestureDetector(this,this);

        //Bottom navigation bar
        BottomNavigationView navBar = findViewById(R.id.navBar);
        navBar.setSelectedItemId(R.id.result);

        navBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                menuItem.setCheckable(true);

                //if history pressed start HistoryActivity and finish current activity
                if(menuItem.getItemId() == R.id.scanner)
                {
                    saveScans();
                    finish();
                    overridePendingTransition(R.anim.left_slide_start,R.anim.left_slide_end);
                }

                return false;
            }
        });

        tvViewConvertSum = findViewById(R.id.textViewConvertSum);
        tvViewConvertSum.setText("0.0");

        spTargetCurrency = findViewById(R.id.spinnerTargetCurrency);
        spTargetCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                convertCurrency();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        analyzeScans();
        getCurrencyRates();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    // Needed overrides for the GestureDetector
    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }
    @Override
    public void onShowPress(MotionEvent motionEvent) {}
    public boolean onSingleTapUp(MotionEvent motionEvent) {return false;}
    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {return false;}
    @Override
    public void onLongPress(MotionEvent motionEvent) {}

    // Detect a swipe to the right. Ensure it is of enough distance and velocity
    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float vX, float vY) {
        float distX = motionEvent2.getX() - motionEvent1.getX();
        float distY = motionEvent2.getY() - motionEvent1.getY();
        if (Math.abs(distX) > Math.abs(distY) && Math.abs(distX) > 50 && Math.abs(vX) > 50) {
            if (distX > 0)
                onSwipeRight();
            return true;
        }
        return false;
    }

    // If swipe right detected then close the Results activity, using an animation to bring back in
    // the Main activity
    public void onSwipeRight(){
        saveScans();
        finish();
        overridePendingTransition(R.anim.left_slide_start,R.anim.left_slide_end);
    }

    void analyzeScans()
    {
        allScans = Money.getSavedScans(this);
        allMoney = new ArrayList<>();

        for (int i = 0; i < allScans.size(); i++)
        {
            allMoney.add(Money.createFromLabel(allScans.get(i)));
        }
        refreshRecycler();
    }

    void refreshRecycler()
    {
        context = getApplicationContext();
        recyclerView = findViewById(R.id.RecyclerViewMoneyList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MoneyAdapter(context, allMoney);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    void getCurrencyRates()
    {
        exchangeRates = new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(this);

        // Use USD as a base and get only CAD and EUR
        String url ="https://api.exchangeratesapi.io/latest?base=USD&symbols=CAD,EUR";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            // Turn the response string into a JSONObject and parse it to find exchange rates
                            JSONObject responseJSON = new JSONObject(response);
                            JSONObject rates = (JSONObject)responseJSON.get("rates");
                            double CADRate = rates.optDouble("CAD");
                            double EURRate = rates.optDouble("EUR");
                            exchangeRates.add( new ExchangeRates("CAD", CADRate));
                            exchangeRates.add( new ExchangeRates("EUR", EURRate));
                            Log.d("MLKIT","CADRate is: "+ CADRate);
                            Log.d("MLKIT","EURRate is: "+ EURRate);
                        } catch (Throwable t) {
                            Log.d("MLKIT", "JSON Error Caught");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                // If the API gave an error then use hardcoded values instead
                exchangeRates.add( new ExchangeRates("CAD", 1.3307230013));
                exchangeRates.add( new ExchangeRates("EUR", 0.9105809506));
                Log.d("MLKIT","Volley Error");
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

        currencies = new ArrayList<>();

        currencies.add("CAD");
        currencies.add("USD");
        currencies.add("EUR");

        // Base rate; will always be 1
        exchangeRates.add( new ExchangeRates("USD", 1));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTargetCurrency.setAdapter(adapter);
    }

    void convertCurrency()
    {
        double usdTotal = 0;
        double convertResult;
        String convertSign;

        for (int i = 0; i < allMoney.size(); i++)
        {
            for (int k = 0; k < exchangeRates.size(); k++)
            {
                if(exchangeRates.get(k).targetCurrency.equals(allMoney.get(i).currencyCode))
                {
                    usdTotal += (allMoney.get(i).value/exchangeRates.get(k).rate);
                }
            }
        }

        if (spTargetCurrency.getSelectedItem().toString().equals("USD") || spTargetCurrency.getSelectedItem().toString().equals("CAD")){
            convertSign = "$";
        }else{
            convertSign = "€";
        }

        convertResult = convertFromUSD(usdTotal, spTargetCurrency.getSelectedItem().toString());

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        decimalFormat.format(convertResult);

        tvViewConvertSum.setText(convertSign + decimalFormat.format(convertResult));
    }

    double convertFromUSD(double usdTotal, String targetCurrency)
    {
        for (int i = 0; i < exchangeRates.size(); i++)
        {
            if(exchangeRates.get(i).targetCurrency.equals(targetCurrency))
            {
                return (usdTotal*exchangeRates.get(i).rate);
            }
        }

        return usdTotal;
    }

    void updateMoney(int index, Money money)
    {
        if(money.currency.equals("Canadian"))
        {
            money.currencyCode = "CAD";
            money.sign = "$";
        }

        else if(money.currency.equals("United States"))
        {
            money.currencyCode = "USD";
            money.sign = "$";
        }

        else if(money.currency.equals("Euro"))
        {
            money.currencyCode = "EUR";
            money.sign = "€";
        }

        money.label = money.currencyCode + money.value;

        allMoney.set(index, money);

        for(int i = 0; i < allMoney.size(); i++) {
            Log.d("MLKIT", "test: " + allMoney.get(i).label);
        }

        convertCurrency();
        refreshRecycler();
    }

    void deleteMoney(int index){
        allMoney.remove(index);
        convertCurrency();
        refreshRecycler();
    }

    @Override
    public void onMoneyClick(View view, final int position)
    {
        ArrayList<String> currencies = new ArrayList<>();
        values = new ArrayList<>();
        selectedMoney = adapter.getItem(position);

        LinearLayout dialogLayout = new LinearLayout(this);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);

        spDialogCurrency = new Spinner(this);
        spDialogValue = new Spinner(this);

        spDialogCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setValues();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        currencies.add("Canadian");
        currencies.add("United States");
        currencies.add("Euro");

        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, currencies);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDialogCurrency.setAdapter(spinnerAdapter);
        spDialogCurrency.setSelection(spinnerAdapter.getPosition(selectedMoney.currency));

        spDialogCurrency.setPadding(0,50,0,50);

        values.add("5");
        values.add("10");
        values.add("20");

        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, values);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDialogValue.setAdapter(spinnerAdapter);
        spDialogValue.setSelection(spinnerAdapter.getPosition(String.valueOf(selectedMoney.value)));

        dialogLayout.addView(spDialogCurrency);
        dialogLayout.addView(spDialogValue);
        dialogLayout.setPadding(40,10,40,10);

        dialog = new AlertDialog.Builder(this)
                .setTitle("Update " + selectedMoney.currency + " " + selectedMoney.sign + selectedMoney.value)
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateMoney(position, new Money(
                                spDialogCurrency.getSelectedItem().toString(),
                                Integer.valueOf(spDialogValue.getSelectedItem().toString())));
                    }
                })
                .setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteMoney(position);
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void onClear(View view)
    {
        allMoney.clear();
        Money.clearScans(this);
        tvViewConvertSum.setText("0.0");
        spTargetCurrency.setSelection(0);
        refreshRecycler();
    }

    void saveScans()
    {
        Money.clearScans(this);

        for(int i = 0; i < allMoney.size(); i++)
        {
            Money.saveScans(this, i, allMoney.get(i).label);
        }
    }

    @Override
    protected void onPause() {

        saveScans();
        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        saveScans();
        finish();
        overridePendingTransition(R.anim.left_slide_start,R.anim.left_slide_end);
    }

    void setValues()
    {
        values = new ArrayList<>();

        if(spDialogCurrency.getSelectedItem().toString().equals("United States"))
        {
            values.add("1");
        }

        values.add("5");
        values.add("10");
        values.add("20");

        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, values);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDialogValue.setAdapter(spinnerAdapter);

        spDialogValue.setSelection(spinnerAdapter.getPosition(String.valueOf(selectedMoney.value)));

    }

    public void onAboutClick(View view) {
        Intent intent = new Intent(this, ResultsAboutActivity.class);
        startActivity(intent);
    }
}
