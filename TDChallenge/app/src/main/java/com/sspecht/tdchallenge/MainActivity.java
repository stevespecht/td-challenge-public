package com.sspecht.tdchallenge;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.ml.common.FirebaseMLException;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.automl.FirebaseAutoMLLocalModel;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabeler;
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions;


import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {

    ImageView ivSplashScreen;
    Button btnScan;
    TextureView tvPreview;
    BottomNavigationView navBar;
    private GestureDetector gestureDetector;
    int scanIndex;
    boolean splashScreenNeed = true;
    CameraDevice camera;
    CameraManager cameraManager;
    Handler handler;
    HandlerThread cameraThread;
    CameraCaptureSession cameraCaptureSessions;
    CaptureRequest.Builder captureRequestBuilder;
    Size imageDimension;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    FirebaseVisionImageLabeler labeler;
    ArrayList<String> allScans;
    AlertDialog dialog;
    Bitmap pictureTaken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gestureDetector = new GestureDetector(this,this);

        //Bottom navigation bar
        navBar = findViewById(R.id.navBar);
        navBar.setSelectedItemId(R.id.scanner);

        navBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                menuItem.setCheckable(true);

                if(menuItem.getItemId() == R.id.result)
                {
                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }

                return false;
            }
        });


        ivSplashScreen = findViewById(R.id.imageViewSplashScreen);
        btnScan = findViewById(R.id.buttonScan);
        tvPreview = findViewById(R.id.textureViewPreview);
        tvPreview.setSurfaceTextureListener(textureListener);

        startSplashScreen();

        scanIndex = 0;
        allScans = new ArrayList<>();
        Money.clearScans(this);

        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);


        // MLKit
        FirebaseAutoMLLocalModel localModel = new FirebaseAutoMLLocalModel.Builder()
                .setAssetFilePath("MoneyModel/manifest.json")
                .build();

        try {
            FirebaseVisionOnDeviceAutoMLImageLabelerOptions options =
                    new FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder(localModel)
                            .setConfidenceThreshold(0.0f)
                            .build();
            labeler = FirebaseVision.getInstance().getOnDeviceAutoMLImageLabeler(options);
        } catch (FirebaseMLException e) {
            Log.d("MLKIT", "ImageLabeler setup threw exception");
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    // Needed overrides for the GestureDetector
    @Override
    public boolean onDown(MotionEvent motionEvent) {return false;}
    @Override
    public void onShowPress(MotionEvent motionEvent) {}
    public boolean onSingleTapUp(MotionEvent motionEvent) {return false;}
    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {return false;}
    @Override
    public void onLongPress(MotionEvent motionEvent) {}


    // Detect a swipe to the left. Ensure it is of enough distance and velocity
    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float velX, float velY) {
        float distX = motionEvent2.getX() - motionEvent1.getX();
        float distY = motionEvent2.getY() - motionEvent1.getY();
        if (Math.abs(distX) > Math.abs(distY) && Math.abs(distX) > 50 && Math.abs(velX) > 50) {
            if (distX < 0)
                onSwipeLeft();
            return true;
        }
        return false;
    }

    // If swipe left detected then open the Results activity
    public void onSwipeLeft(){
        Intent intent = new Intent(MainActivity.this, ResultActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {}
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {}
    };

    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            camera = cameraDevice;
            createPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            camera.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            camera.close();
        }
    };

    CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            createPreview();
        }
    };

    void startCameraThread() {
        cameraThread = new HandlerThread("Camera Thread");
        cameraThread.start();
        handler = new Handler(cameraThread.getLooper());
    }

    void stopCameraThread()
    {
        cameraThread.quitSafely();

        try
        {
            cameraThread.join();
            cameraThread = null;
            handler = null;
        }

        catch (InterruptedException exception)
        {
            exception.printStackTrace();
        }
    }

    void processPicture()
    {
        if(camera == null)
        {
            return;
        }

        try
        {
            btnScan.setEnabled(false);

            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(camera.getId());

            Size[] jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);

            int width = 640;
            int height = 480;

            if (jpegSizes != null && 0 < jpegSizes.length)
            {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(tvPreview.getSurfaceTexture()));

            final CaptureRequest.Builder captureBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);


            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;

                    try
                    {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);

                        analysePicture(bytes);
                    }

                    finally
                    {
                        if (image != null)
                        {
                            image.close();
                        }
                    }
                }
            };

            reader.setOnImageAvailableListener(readerListener, handler);

            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    createPreview();
                }
            };

            camera.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try
                    {
                        session.capture(captureBuilder.build(), captureListener, handler);
                    }

                    catch (CameraAccessException e)
                    {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {}
            }, handler);
        }

        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    protected void createPreview()
    {
        try {
            SurfaceTexture surfaceTexture = tvPreview.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(surfaceTexture);

            captureRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            camera.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession)
                {
                    if (camera == null)
                    {
                        return;
                    }

                    cameraCaptureSessions = cameraCaptureSession;
                    refreshPreview();

                    if(splashScreenNeed)
                    {
                        splashScreenNeed = false;
                        hideSplashScreen();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession){}

            }, null);
        }

        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    private void openCamera()
    {
        try
        {
            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraManager.getCameraIdList()[0]);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{ Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);

                return;
            }

            cameraManager.openCamera(cameraManager.getCameraIdList()[0], stateCallback, null);
        }

        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    protected void refreshPreview()
    {
        try
        {
            captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, handler);
        }

        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_DENIED)
        {
            Toast.makeText(MainActivity.this, "Camera Permissions Needed", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        startCameraThread();

        if (tvPreview.isAvailable())
        {
            openCamera();
        }

        else
        {
            tvPreview.setSurfaceTextureListener(textureListener);
        }
    }

    @Override
    protected void onPause()
    {
        stopCameraThread();
        super.onPause();
    }


    //----------------------------CUSTOM METHODS----------------------------

    public void onScan(View view)
    {
        processPicture();
    }

    Bitmap rotatePicture(Bitmap bitmap)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    void analysePicture(byte[] bytesPicture)
    {
        Bitmap bitmap = rotatePicture(BitmapFactory.decodeByteArray(bytesPicture, 0, bytesPicture.length));

        pictureTaken = bitmap;

        firebaseImage(pictureTaken);
    }

    void firebaseImage(Bitmap pictureTaken){

        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(pictureTaken);

        labeler.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionImageLabel>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionImageLabel> labels) {
                        for (FirebaseVisionImageLabel label: labels) {
                            String text = label.getText();
                            float confidence = label.getConfidence();
                            Log.d("MLKIT", text + ": " + confidence);
                        }

                        btnScan.setEnabled(true);

                        if(labels.size() > 0 && labels.get(0).getConfidence() > 0.5)
                        {
                            confirmScan(labels.get(0).getText());
                        }

                        else
                        {
                            noBill();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        Log.d("MLKIT", "processImage onFailure Hit");
                    }
                });
    }

    void confirmScan(String label)
    {
        final String scannedLabel = label;

        LinearLayout dialogLayout = new LinearLayout(MainActivity.this);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);

        ImageView imageView = new ImageView(MainActivity.this);
        imageView.setImageBitmap(Bitmap.createScaledBitmap(pictureTaken, 500, 500, false));

        dialogLayout.addView(imageView);
        dialogLayout.setPadding(40,10,40,10);

        Money tempMoney = Money.createFromLabel(label);

        dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(tempMoney.currency + " " + tempMoney.sign + tempMoney.value + " detected.")
                .setView(dialogLayout)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scanIndex++;
                        Money.saveScans(MainActivity.this, scanIndex, scannedLabel);
                    }
                })
                .setNegativeButton(R.string.retake, null)
                .create();

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void noBill()
    {
        LinearLayout dialogLayout = new LinearLayout(MainActivity.this);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);

        ImageView imageView = new ImageView(MainActivity.this);
        imageView.setImageResource(R.drawable.scaninsctructions);

        imageView.setLayoutParams(new LinearLayout.LayoutParams(500,500));


        dialogLayout.addView(imageView);
        dialogLayout.setPadding(40,10,40,10);

        dialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.noBillTitle)
                .setMessage(R.string.noBillMsg)
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void onAboutClick(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    void startSplashScreen()
    {
        ivSplashScreen.setVisibility(View.VISIBLE);
        btnScan.setVisibility(View.INVISIBLE);
        navBar.setVisibility(View.INVISIBLE);
    }

    void hideSplashScreen()
    {
        try
        {
            Thread.sleep(1000);
        }

        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        ivSplashScreen.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out_splash_screen));

        try
        {
            Thread.sleep(1000);
        }

        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        ivSplashScreen.setVisibility(View.INVISIBLE);
        btnScan.setVisibility(View.VISIBLE);
        navBar.setVisibility(View.VISIBLE);
    }

    // Override back pressed so that it does nothing
    @Override
    public void onBackPressed(){}
}