package com.sspecht.tdchallenge;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MoneyAdapter extends RecyclerView.Adapter<MoneyAdapter.ViewHolder> {

    ArrayList<Money> allMoney;
    Context context;
    private MoneyAdapter.ItemClickListener mClickListener;

    public MoneyAdapter(Context context, ArrayList<Money> allMoney)
    {
        this.context = context;
        this.allMoney = allMoney;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView tvCurrency, tvValue;

        public ViewHolder(View view)
        {
            super(view);

            tvCurrency = view.findViewById(R.id.textViewCurrency);
            tvValue = view.findViewById(R.id.textViewValue);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.d("test", "click");
            if (mClickListener != null) mClickListener.onMoneyClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Money getItem(int id) {
        return allMoney.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onMoneyClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new MoneyAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_money_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MoneyAdapter.ViewHolder holder, int position)
    {
        holder.tvCurrency.setText(allMoney.get(position).currency);
        holder.tvValue.setText(context.getString(R.string.valOutput , allMoney.get(position).sign, String.valueOf(allMoney.get(position).value)));
    }

    @Override
    public int getItemCount()
    {
        return allMoney.size();
    }
}