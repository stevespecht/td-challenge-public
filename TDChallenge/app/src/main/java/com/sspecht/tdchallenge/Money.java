package com.sspecht.tdchallenge;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Map;

public class Money
{
    String label;
    String currency;
    String currencyCode;
    int value;
    String sign;

    public Money(String currency, String currencyCode, int value, String label)
    {
        this.currency = currency;
        this.currencyCode = currencyCode;
        this.value = value;
        this.label = label;
    }

    public Money(String currency, String currencyCode, int value, String label, String sign)
    {
        this.currency = currency;
        this.currencyCode = currencyCode;
        this.value = value;
        this.label = label;
        this.sign = sign;
    }

    public Money(String currency, int value)
    {
        this.currency = currency;
        this.value = value;
    }

    public static ArrayList<String> getSavedScans(Context context)
    {
        ArrayList<String> savedScans = new ArrayList<>();

        SharedPreferences settings = context.getSharedPreferences("TDChallengePreferences", Context.MODE_PRIVATE);

        Map<String, ?> preferences = settings.getAll();

        //for each delimited string parse and add to savedPins
        for (Map.Entry<String, ?> pref : preferences.entrySet())
        {
            savedScans.add(pref.getValue().toString());
        }

        return savedScans;
    }

    public static void saveScans(Context context, int index, String scan)
    {
        //get shared preferences and clear
        SharedPreferences settings = context.getSharedPreferences("TDChallengePreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(String.valueOf(index), scan);

        editor.commit();
    }

    public static void clearScans(Context context)
    {
        //get shared preferences and clear
        SharedPreferences settings = context.getSharedPreferences("TDChallengePreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();

        editor.commit();
    }

    public static Money createFromLabel(String label)
    {
        if(label.equals("CAD5"))
        {
            return new Money("Canadian", "CAD", 5, "CAD5", "$");
        }

        else if(label.equals("CAD10"))
        {
            return new Money("Canadian", "CAD", 10, "CAD10", "$");
        }

        else if(label.equals("CAD20"))
        {
            return new Money("Canadian", "CAD", 20, "CAD20", "$");
        }

        else if(label.equals("USD1"))
        {
            return new Money("United States", "USD", 1, "USD1", "$");
        }

        else if(label.equals("USD5"))
        {
            return new Money("United States", "USD", 5, "USD5", "$");
        }

        else if(label.equals("USD10"))
        {
            return new Money("United States", "USD", 10, "USD10", "$");
        }

        else if(label.equals("USD20"))
        {
            return new Money("United States", "USD", 20, "USD20", "$");
        }

        else if(label.equals("EUR5"))
        {
            return new Money("Euro", "EUR", 5, "EUR5", "€");
        }

        else if(label.equals("EUR10"))
        {
            return new Money("Euro", "EUR", 10, "EUR10", "€");
        }

        else if(label.equals("EUR20"))
        {
            return new Money("Euro", "EUR", 20, "EUR20", "€");
        }
        return new Money("", "", 0, "", "");
    }
}
